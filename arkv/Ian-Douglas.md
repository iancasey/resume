
# Ian Douglas
#### Cloud Platforms Engineering & Architecture


*   [201-286-2002](tel:#)
*   [iancaseydouglas@gmail.com](mailto:#)
*   Colorado, USA

- - -

* Expertise in cloud systems engineering, automation, and architecture.

* Seasoned professional with experience running high-impact services on Google Cloud Platform, Microsoft Azure, and Amazon Web Services (primarily AWS).

* Extensive use of DevOps patterns and practices, including infrastructure as code, container orchestration, immutable infrastructure, and continuous delivery.

* Toolset includes Kubernetes (both fullstack and GKE), Elastic Container Service, EC2, Docker, CoreOS, Terraform, Packer, Puppet, Ansible, Hashicorp Vault, Jenkins, and Gitlab CI/CD.

* Proven success designing and coding secure deployments of enterprise identity and business critical systems. Mastery of Bash, Python, and PowerShell

* Faculty for creative problem solving. Adaptive team collaborator with a zeal for new challenges.

- - -

### **Experience**  

#### Financial Engineering Sabbatical
<blockquote><span>
<div style="display: flex; justify-content: space-between;"> 
    <div style="text-align: left"> 8/2021 - Present </div>
    <div style="text-align: right">  </div>
</div>
</span></blockquote> 

Developed models and algorithms for automated trading, using market microstructure, orderflow dynamics, and statistical methods. Milestones included:

*   Coded mean-reversion and delta-neutral trading strategies for multiple asset classes and market conditions
*   Developed a framework for rapid prototyping of new models and strategies
*   Gained proficiency in three new programming languages --- Wolfram Language, PineScript, and MQ4/5

#### Senior Cloud Platforms Engineer
<blockquote><span>
<div style="display: flex; justify-content: space-between;"> 
    <div style="text-align: left"> Pearson Education </div>
    <div style="text-align: right"> 9/2019 - 8/2021 </div>
</div>
</span></blockquote> 

Served as a DevOps Engineer for Pearson Education’s ‘Global Learning Platform’, delivering bespoke platform services to software development teams working to create learning and testing solutions for schools and students worldwide. Highlight include:

*   Contributed to the operation and improvement of the company’s platform delivery service, including 8 customer-facing Kubernetes clusters running on AWS EC2. See the case study featured on kubernetes.io
*   Extended the infrastructure codebase defining Kubernetes cluster components, database services, and other AWS primitives, using Terraform, Terragrunt, Ansible, Python, and Shell scripting, with each infrastructure change treated as a platform version release and deployed continuously via Gitlab CICD
*   Delivered a centralized log aggregation solution, using Fluentd and Elasticsearch, in collaboration with the security team
*   Authored Terraform modules for running serverless Gitlab CICD as an autoscaling service on AWS Fargate

#### DevOps Engineer
<blockquote><span>
<div style="display: flex; justify-content: space-between;"> 
    <div style="text-align: left"> Stanford University </div>
    <div style="text-align: right"> 6/2017 - 9/2019 </div>
</div>
</span></blockquote>  

Served as a DevOps Engineer for University IT Emerging Technologies, an engineering and architecture team working with teams across the University to design new and optimal uses of DevOps and Cloud technologies, in order to reinvent IT at Stanford. Highlights included:

*   Contributed as a technical lead to an extensive project to re-architect the university’s Identity and Access Management stack (Kerberos, LDAP, Shibboleth IdP) as Docker containers, running on Google Kubernetes Engine.
*   Designed and coded a reference architecture for Active Directory in AWS, enabling Stanford to offer AD over the IPv6 Internet with end-to-end IPSec, using certificate based authentication, using Hashicorp Vault to bootstrap PKI.
*   Implemented a Privileged Access Management solution for AWS IAM accounts using Hashicorp Vault, enabling technical teams to generate time-bound access credentials in AWS using their Stanford identities with multifactor authentication. 
*   Transformed traditional EC2-based services into Kubernetes Services, including stateful workloads and data-tier components
*   Authored CI/CD pipelines running in Jenkins on Kubernetes, using Jenkinsfile Groovy, Kaniko, Packer, Masterless Puppet
*   Coded the authorization component of the IAM-to-Cloud project, delivering a stateful containerized implementation of master and replica LDAP databases, using Packer, Puppet, Jenkins, Terraform, and Kubernetes Stateful Sets
*   Developed scripted AWS deployment workflows for several IT infrastructure teams on both AWS and GCP, using PowerShell, Terraform, Make, Hashicorp Vault
*   Wrote Terraform code for Kubernetes clusters running on GKE
*   Coded AWS Autoscaling groups and Elastic Load Balancers with Terraform
*   Authored scripts in Bash, PowerShell, and Python
*   Led workshops to instruct IT teams on AWS, Terraform, and PowerShell

#### Windows Infrastructure Architect
<blockquote><span>
<div style="display: flex; justify-content: space-between;"> 
    <div style="text-align: left"> Stanford University </div>
    <div style="text-align: right"> 5/2016 - 6/2017 </div>
</div>
</span></blockquote> 

Served as the Infrastructure and Security Architect for the Windows Infrastructure team, the central provider of Domain AuthN/AuthZ, Kereberos, and LDAP services for the entire University, serving 70,000+ active users, as well as 5 subdomains hosting the Colleges of Business, Engineering, Medicine, Arts and Sciences, and internal IT services. Highlights included:

*   Designed and coded a reference architecture for Active Directory in AWS, enabling Stanford to offer AD over the IPv6 Internet with end-to-end IPSec, using certificate based authentication, using Hashicorp Vault to bootstrap PKI. For details and code examples, see:

    - [New Directions in Providing Active Directory Domain Services](https://iancasey.gitlab.io/resume/new_directions_in_AD.pdf)
    - [AD over the IPv6 Internet with end-to-end IPSec](https://iancasey.gitlab.io/resume/ad_in_aws.pdf)
    - [Bootstrapping PKI Certificates](https://iancasey.gitlab.io/resume/FetchingCerts.pdf)

*   Provided key contributions to a highest priority initiative to harden the security configuration of 100% of all infrastructure, business, and research servers on the Stanford network
*   Worked closely with unit stakeholders to ensure their successful implementation of mandatory host-level firewall, network firewall, disk encryption, OS and application patching and update compliance, multifactor authentication, and application and service white-listing
*   Implemented threat detection and security intelligence solutions for monitoring Domain controllers and entities
*   Liaised with whitehat contractors to perform extensive penetration testing against AD and other infrastructure servers
*   Advanced the Tier-0, Tier-1, Tier-2 segmentation model for containing unauthorized escalation of privilege for the on-premises Active Directory environment
*   Authored Powershell tools for mechanizing or automating tasks in AD, Azure AD, Office365, and more

#### DevOps / Site Reliability Engineer
<blockquote><span>
<div style="display: flex; justify-content: space-between;"> 
    <div style="text-align: left"> Marvel Heroes - Gazillion </div>
    <div style="text-align: right"> 6/2014 - 12/2015 </div>
</div>
</span></blockquote>                                                                                              

Served as a DevOps Engineer for Marvel Heroes, a massively multiplayer online role-playing game, hosting over 45k users per day. Responsible for the deployment and configuration of servers and server clusters backing each environment in the continuous deployment cycle, from Development and QA, through Staging and Production, on both Linux and Windows platforms, physical and virtual. Developed operational tools for automating the complex change processes required by a weekly production release schedule, utilizing PowerShell remoting, Desired State Configuration, Puppet, and SQL Server Management Objects. In addition to authoring scripts, modules, and automated processes, jointly responsible for the administration of a 96-node production cluster, among many other server resources, including Active Directory, VMware ESX, and Puppet Server, as well as being a key player in live operations and monitoring. Highlights included:

*   Developed a PowerShell module for running order-dependent SQL scripts across multiple database hosts in parallel, using multithreaded processes executed via queued jobs in remote sessions, with running progress sent back to the operator’s console and robust error handling. This module automated the data component of the weekly production game release deployment
*   Contributed to a PowerShell module for orchestrating the Marvel Heroes MMO Game Cluster operations, comprising the management of 20 co-dependent services across multiple nodes, utilizing advanced remoting techniques to enable the graceful startup, shutdown, draining, and querying of the clusters. This module replaced legacy Python scripts, and took cluster shutdown time from 6 minutes to 25 seconds
*   Authored Desired State Configuration (DSC) configuration scripts used for the deployment of the entire fleet of Windows Server 2012 R2 Game Cluster servers, across all development cycle environments
*   Created a Puppet module for the installation and configuration of a 16-node ElasticSearch cluster running on CentOS 6, with a Kibana 4 interface
*   Scripted using SQL Management Objects and PowerShell a battery of production release preflight tests for ensuring database configuration compliance, including tests for required logons, role privileges, replication states, and schema versions
*   Created a Puppet module for the installation and configuration of logstash on dozens of CentOS servers
*   Wrote Puppet configurations for the corporate webservers and store servers running on the Linux / Nginx / MySQL / PHP stack
*   Instituted GitLab for versioning BIND DNS, Puppet, DSC, other code used by DevOps team

#### Systems Engineer                                                                                               
<blockquote><span>
<div style="display: flex; justify-content: space-between;"> 
    <div style="text-align: left"> Two Sigma Investments  </div>
    <div style="text-align: right"> 5/2012 - 6/2014 </div>
</div>
</span></span></blockquote>

Served as a Systems Engineer for a high-tech quantitative hedge fund with over $58 billion in assets under management. Responsible for the configuration management of 1000+ staff workstations and approximately 350 Windows servers. Enterprise and Domain Administrator in a multi-forest, multi-domain, global Active Directory environment. Subject matter expert for Systems Center 2012 and Windows Powershell. Co-administrator of VMware vSphere, Active Directory, HyperV. Technical lead for the Windows Platform Team. Highlights included:

*   Designed and implemented an SCCM 2012 infrastructure spanning 3 datacenters and multiple distribution points, eliminating single points of failure using highly available server clusters and virtual machine replication
*   Scripted the build and deployment of over 1000 highly secure Windows 7 workstations in concert with a company-wide staff relocation
*   Developed the scripted deployment process used for distributing the production trading UI
*   Automated the build of Server 2008R2 Terminal Services servers, Citrix XenApp Servers, Server 2012 R2 member servers
*   Coded in PowerShell server and desktop configuration scripts using one-to-many remoting
*   Implemented a multi-environment patch management solution for desktops and servers
*   Wrote PowerShell scripts for Active Directory, vSphere, and Windows Server administration
*   Configured group policies for Applocker, Firewall, WinRM, and many others
*   Delivered self-service tools for AD group management
*   Deployed a self-service web portal for application installation
*   Authored a PowerShell module with dozens of functions for use by the Service Desk team

#### Systems Engineer
<blockquote><span>
<div style="display: flex; justify-content: space-between;"> 
    <div style="text-align: left"> Columbia University  </div>
    <div style="text-align: right"> 6/2010 - 5/2012 </div>
</div>
</span></blockquote>

Served as the lead Windows Systems and Virtualization Engineer for Columbia University Libraries. Responsible for the configuration management of 1600+ staff and public-facing workstations. Technical team lead for Active Directory Domain Services, VMware vSphere / View, Systems Center, Server 2008, SQL Server, Altiris, and Netapp. Provided tier-2/3 escalation support to service desk personnel. Highlights included:

*   Scripted the layered deployment of six distinct ‘Digital Center’ Windows 7 computing labs, with over 30 industry leading engineering, science, mathematics, and design applications
*   Automated an XP to Windows 7 migration for staff workstations
*   Collaborated on a project of server consolidation onto ESXi / vSphere
*   Designed and configured 3 vSphere High Availability clusters
*   Implemented a DFS domain namespace & replication topology
*   Performed installation and rollout of Systems Center 2007 to replace Altiris 6.9
*   Configured a cross-forest realm trust between an AD Domain and a UNIX realm, enabling single sign-on for domain users using their UNIX credentials
*   Designed and implemented a virtual desktop infrastructure pilot on VMware View 5

#### Systems Administrator
<blockquote><span>
<div style="display: flex; justify-content: space-between"> 
    <div style="text-align: left"> University of Denver </div>
    <div style="text-align: right"> 5/2003 - 1/2010 </div>
</div>
</span></blockquote>

Ensured the continuous uptime and protection of production servers for the university’s libraries. Responsible for the deployment, configuration and software/hardware maintenance of 21 physical servers housed in the data center, as well as numerous virtual servers, 3 computer labs, dozens of public kiosk terminals, several point of service stations, and hundreds of staff workstations and VIP laptops. Made strategic recommendations on the specific application of technologies to service objectives. Trained and supervised helpdesk consultants and provided tier-2 support to faculty and staff. Planned, documented, and chiefly implemented the ongoing rotation of server and client hardware and software. Performed database development and web-application programming for various projects of the Digital Library Initiative. Highlights included:

*   Implemented initial domain rollout and group policy configuration
*   Automated or centralized many system administration processes
*   Spearheaded an initiative to consolidate multiple physical servers onto virtual infrastructure
*   Designed and coded a web-based content management system for streaming audio
*   Trained and supervised technical staff
*   Collaborated and maintained relationships with project stakeholders across the university

#### Systems Administrator
<blockquote><span>
<div style="display: flex; justify-content: space-between;"> 
    <div style="text-align: left"> Skandinaviske Skoledagbøker </div>
    <div style="text-align: right"> 9/2000 - 5/2003 </div>
</div>
</span></blockquote>

Administered IT systems operations for a small publishing company with five sites in three countries (US, Norway, Germany). Responsibilities included the proactive maintenance of servers, workstations, and network devices, administration of Active Directory, SQL databases, DNS, VPN, IIS and FTP servers, proxy/firewall servers, and Exchange mail servers, as well as the implementation and maintenance of distributed file system, and the application of security updates and daily backups. Highlights included:

*   Designed, coded, and administered company databases
*   Planned and oversaw development of a custom software solution addressing print-production bottlenecks, resulting in a 700% increase in workflow throughput
*   Implemented a domain migration from NT4 to Windows 2000
*   Supervised a four-person technical services team

## Skills

![cloud of clouds](cloud.png)

<div style="display: flex; justify-content: space-around;"> 
<table>    
<thead>
<tr>
<th style="text-align:center">Paradigms</th>
<th style="text-align:center">Platforms</th>
<th style="text-align:center">Tools</th>
<th style="text-align:center">Software</th>
<th style="text-align:center">OS</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:center">GitOps</td>
<td style="text-align:center">AWS</td>
<td style="text-align:center">Terraform</td>
<td style="text-align:center">Gitlab CICD</td>
<td style="text-align:center">Debian</td>
</tr>
<tr>
<td style="text-align:center">Infrastructure-as-Code</td>
<td style="text-align:center">Kubernetes</td>
<td style="text-align:center">Python</td>
<td style="text-align:center">Vault</td>
<td style="text-align:center">Ubuntu</td>
</tr>
<tr>
<td style="text-align:center">Continuous Delivery</td>
<td style="text-align:center">EC2</td>
<td style="text-align:center">Powershell</td>
<td style="text-align:center">Jenkins</td>
<td style="text-align:center">Centos</td>
</tr>
<tr>
<td style="text-align:center">Agile</td>
<td style="text-align:center">GKE</td>
<td style="text-align:center">Bash</td>
<td style="text-align:center">Azure AD</td>
<td style="text-align:center">Alpine</td>
</tr>
<tr>
<td style="text-align:center">Zero Trust</td>
<td style="text-align:center">vSphere</td>
<td style="text-align:center">Terragrunt</td>
<td style="text-align:center">Docker</td>
<td style="text-align:center">ServerCore</td>
</tr>
<tr>
<td style="text-align:center">Immutable Infrastructure</td>
<td style="text-align:center">ECS</td>
<td style="text-align:center">Packer</td>
<td style="text-align:center">SQL Server</td>
<td style="text-align:center">Nano</td>
</tr>
<tr>
<td style="text-align:center">Container Orchestration</td>
<td style="text-align:center">GCP</td>
<td style="text-align:center">Ansible</td>
<td style="text-align:center">Elasticsearch</td>
<td style="text-align:center">Windows Server</td>
</tr>
<tr>
<td style="text-align:center">Iterative Transformation</td>
<td style="text-align:center">Azure</td>
<td style="text-align:center">Puppet</td>
<td style="text-align:center">Kerberos</td>
<td style="text-align:center">CoreOS</td>
</tr>
</tbody>
</table></div>
### Education

<blockquote><span>
<div style="display: flex; justify-content: space-between;"> 
    <div style="text-align: left"> University of Denver </div>
    <div style="text-align: right"> 1996 - 2000 </div>
</div>
</span></blockquote> 
<div style="text-align: left"> Bachelors in Mathematics & Philosophy </div>
<div style="text-align: left"> Double Major </div>

