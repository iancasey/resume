<!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Toward a Zero-Trust Mesh for Service Communications</title>
        <style>
/* From extension vscode.github */
/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

.vscode-dark img[src$=\#gh-light-mode-only],
.vscode-light img[src$=\#gh-dark-mode-only] {
	display: none;
}

</style>
        
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/Microsoft/vscode/extensions/markdown-language-features/media/markdown.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/Microsoft/vscode/extensions/markdown-language-features/media/highlight.css">
<style>
            body {
                font-family: -apple-system, BlinkMacSystemFont, 'Segoe WPC', 'Segoe UI', system-ui, 'Ubuntu', 'Droid Sans', sans-serif;
                font-size: 14px;
                line-height: 1.6;
            }
        </style>
        <style>
.task-list-item {
    list-style-type: none;
}

.task-list-item-checkbox {
    margin-left: -20px;
    vertical-align: middle;
    pointer-events: none;
}
</style>
        
    </head>
    <body class="vscode-body vscode-light">
        <h1 id="toward-a-zero-trust-mesh-for-service-communications">Toward a Zero-Trust Mesh for Service Communications</h1>
<p><em><strong>Secure Service-to-Service Communications over the IPv6 Internet with end-to-end IPSec and ephemeral certificates</strong></em></p>
<p>Ian Douglas, for Stanford University</p>
<h2 id="tldr"><strong>TLDR</strong></h2>
<p><em><strong><a href="#the-problem">The Problem</a></strong></em></p>
<p><em><strong><a href="#the-solution">The Solution</a></strong></em></p>
<h4 id="kerberos-before"><em>Kerberos, before</em></h4>
<p><img src="https://iancasey.gitlab.io/resume/cutie_cerberos.png" alt="Kerberos" title="Kerberos in a Basket"></p>
<h2 id="background-and-a-parallel-project"><strong>Background and a Parallel Project</strong></h2>
<p>As part of a multiyear initiative to modernize IT infrastucture services, my team was responsible for delivering the architecture and reference implementation of the first core infrastructure service stack to be deployed to the cloud, positioned as a model and to showcase our so-called Cloud Transformation.</p>
<p>While we saw this as a greenfield opportunity to evolve our service portfolios' deployment and operating models, most important to the Regents was that core services be available even if the campus was entirely cut-off, as in the event of an earthquake (not improbable given our proximity to the San Andreas fault). Services needed to be fully operational even if all connections to the on-prem network were severed. Additionally, we needed to ensure that we could serve our highly capable and technical community without having to micromanage IP Address space or require close coordination with central IT. In service of these requirements, we enshrined three principles:</p>
<p><em><strong><a href="#">Service Design Principles</a></strong></em></p>
<ol>
<li>Globally addressable
: Services should be discoverable via DNS records pointing to public, globally routable IP addresses</li>
<li>Private-IP Agnostic
: Services should be able to service client VPCs without regard to their private IP addressing scheme --even VPCs with overlapping CIDRs</li>
<li>Self-sufficient Islands
: Services should be <em>loosely connected</em> to on-prem services and be fully operational if all connections to the campus network are severed</li>
</ol>
<h3 id="identity--authnz-service-stack"><strong>Identity &amp; AuthN/Z Service Stack</strong></h3>
<p>The first service (suite of services) to be re-architected for the cloud was the Identity Authentication &amp; Authorization Stack, a lynchpin for all of the services that would follow. The stack consisted of the following core application protocols:</p>
<ul>
<li>Kerberos</li>
<li>LDAP</li>
<li>SAML</li>
</ul>
<p>We will leave SAML out of the discussion here, focusing instead on LDAP &amp; Kerberos which consisted of:</p>
<ul>
<li>Heimdal Kerberos: a Kerberos-v5 Realm of the MIT/UNIX variety, running on Debian Linux</li>
<li>Open LDAP: Open Source LDAP, also on Debian</li>
<li>ADDS Kerberos: a Kerberos-v5 Domain of the Microsoft strain, proprietary but compliant with Kerberos RFCs</li>
<li>ADDS LDAP, co-located with Kerberos services, together comprising the Active Directory Services (ADDS)</li>
</ul>
<h3 id="notable-complexity"><strong>Notable complexity</strong></h3>
<p>Both Kerberos and LDAP have database backends that are distributed across any number of hosts.</p>
<p>In the case of LDAP, that database is replicated in a variety of modes, supporting both single-master and multi-master topologies, as well as pub/sub hierarchies and read-only replica pools, with support for flexible publishers that shadow authoritative masters and which can assume the master role as needed.</p>
<p>ADDS LDAP has had a much faster development cycle, pioneering multimaster replication with its innovative replication protocol that utilizes database partioning and 5 different types of master roles, which can be assumed by any of the ADDS server instances (except read replicas).</p>
<p>Kerberos can be complex as well. Among organizations that each separately host Kerberos services, symmeteric encryption schemes may be used to establish Trust relationships between separate Domains and Realms, organizing them in tree-like hierarchies called Forests, so that security relationships can be formed that are either uni- or bi-directional, optionally transitive or one-to-one, and entities from one realm can (if authorized) operate in another realm with seamless and transparent SSO.</p>
<p>The degree of complexity varies by the particular implementation. In Stanford's case, it had a nontrivial amount of wrinkles: the ADDS Kerberos Forest had 5 domains and a root domain that trusted the Heimadal Realm in a unidirectional, non transitive trust relationship. Password changes and other identity-related properties were replicated into ADDS via this trust. Additionally, the ADDS forest trusted a Security team domain in a relationship known as Red Forest.</p>
<p>Moreover, OpenLDAP and ADDS LDAP were replica partners.</p>
<p>Add to these complexities the fact that this was the first production cloud deployment of core infra services --and that the data in these systems had the <em>highest possible risk classification</em>-- one would rightly infer that there were a lot of eyes watching with keen interest.</p>
<h2 id="spoiler-alert"><strong>Spoiler Alert</strong></h2>
<p>We delivered the whole shebang, flawlessly: rolled it out over the course of 8 hours, first a Canary to 3%, then 5%...10%, gradually flowing in more traffic.</p>
<p>What's more, we delivered all the linux-based services (Kerberos, LDAP, Shibolleth) as containerized workloads on Kubernetes.</p>
<h3 id="my-contribution"><strong>My contribution</strong></h3>
<ul>
<li>
<p>technical lead on the LDAP implementation. True to our charge, I 'transformed' what was essentially a distributed database into a Kubernetes 'Stateful Set' service, along with many required companion services that handled care and feeding of the data, its schema, configuration files, as well as rotating its secrets, like certificates and keytabs. This came with CI pipelines for the image build and for mutating configuration files per deployment environment. And Yes! extensive documentation including an operations runbook. And even though the Kubernetes implementation was successful, I also delivered fully tested terraform modules for running each of the service roles (ie, masters or replicas) more conventionally, as a load-balanced cluster on EC2 with horizontal autoscaling for the replica pools, in case we needed to pivot due to some unknown unknown.</p>
</li>
<li>
<p>architect and technical lead on the ADDS implementation. I produced the proof-of-concept and reference implementation a full year before joining the cloud enablement team full-time. And when we were done with the AuthN/AuthZ Kubernetes deployment, I was asked to carry ADDS toward production, with the intermediate goal of deploying it into our UAT environment, one step below production, so that it could undergo further pentesting.</p>
</li>
</ul>
<p>While these projects offered many enjoyable challenges, there was one whose solution was particularly interesting, in that it can be generalized for any service that requires highly secure service-to-service communications between disconnected VPCs, even in the case of overlapping CIDRs. As an added benefit, its setup is decentralized, and costs nothing. What's more, it is cross-regional --unlike both of the conventional solutions, which are either confined to the local region, centralized, or require constant payment to the piper.</p>
<h2 id="the-problem"><strong>The Problem</strong></h2>
<p>How might a server in an arbitrary VPC and account (either linux or windows) join the cloud-resident ADDS Domain, using the same identities as used on premises, in a secure and confidential manner?</p>
<p>**<em>(and adhere to the service design principles)</em></p>
<h3 id="the-user-story--client-use-case"><strong>The User Story / Client use case</strong></h3>
<p><em>Specific use-cases with immediate priority</em></p>
<ul>
<li>As previously highlighted, service-to-service communications were required between the win/nix ldap directories, and between the win/nix kerberos realms</li>
<li>linux servers running MSSQL services with Kerberos-based authentication (ADDS) for admin accounts, replication, and database user accounts, operated by the Endowment's internal Hedge Fund</li>
</ul>
<p><em>General use-cases</em></p>
<ul>
<li>windows or linux servers needed to be accessed by users with their stanford identities (SSH, RDP, etc)</li>
<li>any service using Kerberos or Kerberized LDAP, or any server needing to be managed by domain group policies</li>
</ul>
<h2 id="one-does-not-simply-expose-ad-to-the-internet"><strong>One Does Not Simply <em>Expose AD</em> to the Internet</strong></h2>
<p>A conventional approach would take one of two forms:</p>
<h3 id="vpc-peering"><strong>VPC Peering</strong></h3>
<p>This violates (2) of our desired design patterns, since peering requires that there be no overlapping IP space.</p>
<p>Also, peering topologies are limited to the local region and have a maximum of 125 active peering connections, with network performance significantly impacted at 16 connections, so they are not well-suited to a highly distributed model.</p>
<h3 id="transit-gateway"><strong>Transit Gateway</strong></h3>
<p>While transit gateway supports thousands of VPCs, it requires that the connected VPCs have disjoint CIDRs, violating principle (2) once again.</p>
<h3 id="end-to-end-encryption-and-globally-routable"><strong>End-to-End Encryption and Globally routable</strong></h3>
<blockquote>
<p>4,722,366,482,869,645,213,696</p>
<p>2^(128-56): size of your VPC's stable allocation of public IPv6 addresses within a /56 CIDR block, or</p>
<p>4 sextillion 722 quintillion 366 quadrillion 482 trillion 869 billion 645 million 213 thousand 696</p>
</blockquote>
<h2 id="the-solution"><strong>The Solution</strong></h2>
<p>Provide services over public IPv6, using peer-to-peer IPSec to
ensure confidentiality. This confers global routablity and end-to-end encryption, and it is consistent with all three principles.</p>
<p>IPSec policies are typically implemented in either 'request' or 'require' mode, scoped to some remote CIDR. Setting this to 'require' referenced to the /56 assigned to the partner VPC is enough to force IPSec, so long as each server has an x509 certificate issued by the same CA (or issued by a CA within the chain of trust).</p>
<h3 id="short-lived-certificates-and-delivering-secret-zero"><strong>Short lived Certificates and Delivering Secret Zero</strong></h3>
<p>The solution as stated offers yet more puzzles to solve:</p>
<p><strong><em>Which authority to use for the IPSec certificates?</em></strong></p>
<p><strong><em>By what mechanism bootstrap servers with certificates?</em></strong></p>
<p><strong><em>How to guarantee Secure Introduction?</em></strong></p>
<p>Using the conventional Certificate Authority often employed in domain environments, ADDS Certificate Services, requires that the server already be joined to the domain, but because we want to secure communications before, during, and after the domain join process, we need a certificate enrollment process that has no dependencies on ADDS.</p>
<p>I evaluated two different solutions for providing x509 certificates automatically during the server bootstrap process: Let's Encrypt (with DNS validation) and Hashicorp Vault. Both provide certificates that can be used for IPSec, and an enrollment process that can be entirely automated.</p>
<h3 id="lets-encrypt"><strong>Let's Encrypt</strong></h3>
<p>The Let's Encrypt solution required that the servers be launched in an EC2 Role that allows creation of DNS TXT records in a specific Route 53 zone, so that the CA could validate the certificate request. While scripting this with aws cli to run as part of machine startup and at regular intervals was straightforward, allowing DNS write privileges was too permissive, and configuring and updating such an IAM policy for cross-account access required an unacceptable amount of ongoing administrative overhead.</p>
<h3 id="hashicorp-vault"><strong>Hashicorp Vault</strong></h3>
<p>A solution using Hashicorp Vault's PKI engine was favored, since my team already operated a highly available Vault cluster. This solution required that the Vault PKI backend be configured with a
role to issue certificates, an authorization policy be written to grant access to the certificate
issuer endpoint, and that a specific approle be created to authenticate api requests to assume
said policy's role.</p>
<h3 id="secure-introduction"><strong>Secure Introduction</strong></h3>
<p>One particular problem that always needs to be solved when integrating external secrets stores is the problem of <em><a href="https://www.vaultproject.io/guides/identity/secure-intro.html">secure introduction</a></em>:</p>
<p>Bob: <em>&quot;How does a would-be secret consumer prove that it is the legitimate recipient of a secret?&quot;</em></p>
<p>Alice: <em>&quot;By providing another (pre-shared) secret of course!&quot;</em></p>
<p>But this begs the question,</p>
<blockquote>
<p><em>How does one obtain the secret that obtains the secret?</em></p>
</blockquote>
<p>This is the crux of the problem of secure introduction.</p>
<h3 id="obtaining-secret-zero"><strong>Obtaining Secret Zero</strong></h3>
<p>In the specific case of Vault, the pre-shared secret consists of a RoleID and SecretID, which are included in the initial request to Vault, and if deemed legitimate, Vault returns a token containing data specifying the scope of access for any subsequent requests using this token.</p>
<p>We chose a 'platform integration' approach, delegating secure introduction to the trusted mechanisms available to AWS EC2, namely Systems Manager Parameter Store, KMS, and IAM. This resource provides an encrypted key-value store with fine-grained access controls using IAM polices. When a server is launched in an EC2 role with a certain IAM Policy, it is allowed to read and decrypt the particular Parameter Store values containing the RoleID and SecretID.</p>
<p>Once 'secret zero' has been delivered to the server in the form of a RoleID and SecretID,
obtaining a certificate from Vault is straightforward, and requires no special client software,
since all of Vault's capabilities are accessible through the REST API. The entire certificate
request process can be performed using curl.</p>
<p>Having received and decrypted the SecretID and RoleID from the Parameter Store, the server then authenticates to
Vault using an HTTP POST request, and once the request is validated and assigned to an
approle, Vault returns a limited scope token to the client. This token is placed into the header
of a subsequent POST request against the certificate issuer endpoint, and if the request meets
all required criteria, a certificate is issued in PEM block format. The PEM block contains the
requestor's new certificate, the associated private key, and the certificate of the issuing CA.</p>
<p>For a code example, see the following, a function for retrieving data from the Parameter Store,
requesting a certificate from Vault, and importing it into the local certificate store. Note that this
runs in a userData script on the target AWS EC2 instance, as the server is being launched for the
first time. Since we use Terraform to define and deploy our IAAS infrastructure, we leverage
Terraform's template abstraction to render the userdata dynamically and to provide the
required values to the function parameters.</p>
<h4 id="kerberos-after"><em>Kerberos, after</em></h4>
<p><img src="https://iancasey.gitlab.io/resume/armored_kerberos.png" alt="Kerberos" title="Kerberos in Armor"></p>
<p>Having obtained a certificate, the server may now establish an IPSec policy scoped to the IPv6 CIDR of the partner VPC network. Presuming that both sides have
machine certificates signed by the Vault CA, and that the CA's certificate has been imported
into the trusted store, a cryptographic set should be negotiated and an IPsec channel
established.</p>
<h2 id="coda"><strong>Coda</strong></h2>
<p>The potential use-cases for this solution are various, since it allows for on-demand, full-mesh networking with global span.</p>
<p>To operationalize this solution, the bootstrapping scripts could be rewritten in Golang and delivered as a cross-platform binary, and the IAM/KMS/ParamaterStore requirements could be packaged in a reusable Terraform module or as a CDK Stack.</p>

        
        
    </body>
    </html>