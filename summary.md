- Expertise in cloud infrastructure and Kubernetes
- Fluent in Python, Bash, and PowerShell
- Baremetal Deployment Veteran
- Proficiency in coding Terraform, CDK, Boto3, Helm, CLIs
- Deep Experience with Vault and secrets management
- Seasoned DevOps practitioner of GitOps and CI/CD


Platform Engineer with expertise in cloud infrastructure & Kubernetes, fluent in Python, Bash, IaC Toolsets, experience with Cloud Transformation

A  Platform Engineer with expertise in cloud infrastructure and Kubernetes, fluency in Python, Bash, and IaC toolsets, and experience contributing to successful cloud transformation initiatives, as well as delivering Internal Developer Platforms.

An astute Platform Engineer specializing in cloud infrastructure and Kubernetes, proficient in Python, Bash, and IaC Toolsets, with a proven track record delivering Internal Developer Platforms as well as driving Digital transformation Initiatives

A versatile Platform Engineer with expertise in cloud infrastructure and Kubernetes, fluent in Python, Bash, and Infrastructure as Code, with proven success delivering Internal Developer Platforms and digital transformation initiatives.

A versatile Platform Engineer with demonstrated success delivering Internal Developer Platforms and implementing digital transformation initiatives.

medium-shot painted portrait in the style of Rembrandt of a plump frog in a victorian-era suit, sitting on a small stool while looking at the viewer with a bemused expression, meme art, massurrealism, no hat, no table --v 

 painted portrait in the style of [Rembrandt] of a plump frog in a victorian-era suit, sitting on a chair while looking at the viewer, meme art, massurrealism, --s 750 --v 5.1 --no hat table

painted portrait in the style of Rembrandt of a plump frog in a victorian-era suit, sitting on a chair while looking at the viewer, meme art, massurrealism --s 750 --v {5,5.1} --no hat table

labyrinth made out of belgian waffle emblem, kitschy vintage retro simple --no shading detail ornamentation realistic color

